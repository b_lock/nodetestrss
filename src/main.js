/* eslint-disable no-console */
import express from 'express';
import http from 'http';

const libxmljs = require('libxmljs');
const reqF = require('req-fast');
require('twig');

const app = express();
app.use(express.static('static'));
app.set('twig options', {
  strict_variables: false,
});

async function getRss(url) {
  return new Promise((resolve, reject) => {
    const options = {
      url,
      method: 'GET',
    };
    reqF(options, (err, res) => {
      if (err) {
        console.log('[ERROR]', err.message);
        reject(new Error(`Failed to load page, status code: ${res.statusCode}`));
      } else {
        const data = res.body;
        resolve(data);
      }
    });
  });
}

async function getNews(count) {
  const link = 'https://www.057.ua/rss';
  let news;
  try {
    news = await getRss(link);
  } catch (err) {
    console.error(err);
    Error(err);
  }
  const xmlDoc2 = libxmljs.parseXmlString(news);
  const items = xmlDoc2.find('//item');
  let result = [];
  if (items) {
    const lastNews = items.slice(0, count);

    result = lastNews.map((item) => {
      const contentText = item.find('*').find(el => el.name() === 'encoded');
      return {
        title: item.get('title').text(),
        link: item.get('link').text(),
        content: contentText ? contentText.text() : '',
      };
    });
  }
  return result;
}

app.use('/news/:count', async (req, res) => {
  const news = await getNews(req.params.count);
  res.render('main.twig', { news });
});

app.use((req, res, next) => {
  next(404);
});

// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next) => {
  console.log('error originalUrl', req.originalUrl);
  console.error(err);
  const error = {};
  if (err === 404) {
    error.text = 'Page not Found';
    error.status = '404';
    res.status(404);
  } else {
    error.text = 'Error';
    error.status = '500';
    res.status(500);
  }
  res.render('error.twig', {
    error,
  });
});

const server = http.createServer(app);
server.listen(3000, () => console.log('Listening on http://localhost:3000'));
